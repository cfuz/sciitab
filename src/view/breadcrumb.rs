use super::{print, IView};
use crate::model::tab::Tab;
use std::cell::RefCell;
use std::rc::Rc;

pub struct BreadCrumb {
    names: Vec<(String, bool)>,
    pos: (usize, usize),
    len: usize,
}

impl BreadCrumb {
    pub fn new(model: &Rc<RefCell<Tab>>) -> Self {
        Self {
          names: vec![(model.borrow().name.clone(), true)],
          pos: Default::default(),
          len: 1,
        }
    }
}

impl IView for BreadCrumb {
    fn show(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let (mut content, mut up, mut mid, mut low) = ("".to_string(), "".to_string(), "".to_string(), "".to_string());
        for (name, state) in self.names.iter() {
          if !state {
            up += &format!("{0:-<1$}", "", name.len() + 4);
            mid += &format!("| {} |", name.clone());
          } else {
            up += &format!("{0:<1$}", "", name.len() + 4);
            mid += &format!("| {} |", console::style(name.clone()).bold());
          }
          low += &format!("+{0:-<1$}+", "", name.len() + 2);
        }
        if up.len() < 80 {
            up += &format!("{0:-<1$}", "", 80 - content.len());
        }
        for s in vec![up, mid, low].iter() {
            content += s.as_str();
            content += "\n";
        }
        let buff_len = content.split('\n').collect::<Vec<_>>().len();
        if buff_len != self.len {
            self.len = buff_len;
            self.pos.1 = self.pos.0 + self.len;
        }
        print(&content);
        Ok(())
    }

    fn set_start_pos(&mut self, start_pos: usize) {
        if start_pos != self.pos.0 {
            self.pos.0 = start_pos;
            self.pos.1 = self.pos.0 + self.len;
        }
    }

    fn start_pos(&self) -> usize {
        self.pos.0
    }

    fn end_pos(&self) -> usize {
        self.pos.1
    }

    fn len(&self) -> usize {
        self.len
    }
}
