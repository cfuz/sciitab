use super::{print, IView};

const TITLE: &str = "
┌─┐┌─┐┬┬┌┬┐┌─┐┌┐
└─┐│  ││ │ ├─┤├┴┐
└─┘└─┘┴┴ ┴ ┴ ┴└─┘
";

#[derive(Default)]
pub struct Title {
    pos: (usize, usize),
    len: usize,
}

impl Title {
    pub fn new() -> Self {
        Default::default()
    }
}

impl IView for Title {
    fn show(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        print(TITLE);
        let buff_len = TITLE.split('\n').collect::<Vec<_>>().len();
        if buff_len != self.len {
            self.len = buff_len;
            self.pos.1 = self.pos.0 + self.len;
        }
        Ok(())
    }

    fn set_start_pos(&mut self, start_pos: usize) {
        if start_pos != self.pos.0 {
            self.pos.0 = start_pos;
            self.pos.1 = self.pos.0 + self.len;
        }
    }

    fn start_pos(&self) -> usize {
        self.pos.0
    }

    fn end_pos(&self) -> usize {
        self.pos.1
    }

    fn len(&self) -> usize {
        self.len
    }
}
