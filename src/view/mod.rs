use crate::TERM;
use actions::Actions;
use ascii_tab::AsciiTab;
use breadcrumb::BreadCrumb;
use enum_dispatch::enum_dispatch;
use title::Title;

pub mod actions;
pub mod ascii_tab;
pub mod title;
pub mod breadcrumb;
// pub mod save;

#[enum_dispatch]
pub enum View {
    AsciiTab,
    Title,
    Actions,
    BreadCrumb
}

#[enum_dispatch(View)]
pub trait IView {
    fn show(&mut self) -> Result<(), Box<dyn std::error::Error>>;
    fn set_start_pos(&mut self, start_pos: usize);
    fn start_pos(&self) -> usize;
    fn end_pos(&self) -> usize;
    fn len(&self) -> usize;
    fn clear(&self) -> Result<(), Box<dyn std::error::Error>> {
        TERM.move_cursor_to(0, self.end_pos())?;
        TERM.clear_last_lines(self.len())?;
        Ok(())
    }
}

pub fn print(data: &str) {
    TERM.write_line(data).expect("Couldn't write in terminal!");
}
