use super::{print, IView};
use crate::model::tab::Tab;
use std::cell::RefCell;
use std::rc::Rc;

const NO_DATA: &str = "
+----------+
| No data! |
+----------+
";

pub struct AsciiTab {
    model: Rc<RefCell<Tab>>,
    pos: (usize, usize),
    len: usize,
}

impl AsciiTab {
    pub fn new(model: &Rc<RefCell<Tab>>) -> Self {
        Self {
            model: model.clone(),
            pos: Default::default(),
            len: Default::default(),
        }
    }
}

impl IView for AsciiTab {
    fn show(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let tab = self.model.borrow();
        let content = if tab.content.is_empty() {
            String::from(NO_DATA)
        } else {
            let (bold, std) = (
                console::Style::new().bg(console::Color::White).black(),
                console::Style::new(),
            );
            let (mut sep, mut buff) = (String::from(""), String::from(""));
            for idx in 0..tab.num_cols {
                sep += &format!("+{1:-<0$}", tab.widths[idx] + 2, "");
            }
            sep += "+\n";
            buff += &sep;
            for (row_idx, row) in tab.content.iter().enumerate() {
                for (col_idx, cell) in row.iter().enumerate() {
                    let formatted_cell = format!(" {}", if cell.is_empty() {
                        if row_idx == 0 && tab.header {
                            "-".to_string()
                        } else {
                            cell.clone()
                        }
                    } else {
                        cell.clone()
                    });
                    let mut style = if &(row_idx, col_idx) == &tab.current {
                        if row_idx == 0 && tab.header {
                            console::Style::new().bg(console::Color::Green).black().apply_to(formatted_cell)
                        } else {
                            bold.apply_to(formatted_cell)
                        }
                    } else {
                        std.apply_to(formatted_cell)
                    };
                    if row_idx == 0 && tab.header && &(row_idx, col_idx) != &tab.current {
                        style = style.green()
                    };
                    buff += &format!("|{1:<0$}", tab.widths[col_idx] + 2, style);
                }
                buff += "|\n";
                if (row_idx == 0 && tab.header) || row_idx == tab.content.len() - 1 {
                    buff += &sep;
                }
            }
            buff
        };
        let buff_len = content.split('\n').collect::<Vec<_>>().len();
        if buff_len != self.len {
            self.len = buff_len;
            self.pos.1 = self.pos.0 + self.len;
        }
        if self.len != 0 {
            self.clear()?;
        }
        print(&content);
        Ok(())
    }

    fn set_start_pos(&mut self, start_pos: usize) {
        if start_pos != self.pos.0 {
            self.pos.0 = start_pos;
            self.pos.1 = self.pos.0 + self.len;
        }
    }

    fn start_pos(&self) -> usize {
        self.pos.0
    }

    fn end_pos(&self) -> usize {
        self.pos.1
    }

    fn len(&self) -> usize {
        self.len
    }
}
