use super::{print, IView};

const MAX: usize = 4;

pub struct Actions {
    actions: Vec<String>,
    pos: (usize, usize),
    len: usize,
}

impl Actions {
    pub fn new(actions: &Vec<String>) -> Self {
        Self {
            actions: actions.clone(),
            pos: Default::default(),
            len: {
                let rem = actions.len() % MAX;
                (actions.len() / MAX) + if rem == 0 { 1 } else { 0 }
            },
        }
    }
}

impl IView for Actions {
    fn show(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        // if self.len != 0 {
        //     self.clear()?;
        // }
        let mut content: String = String::from("");
        for (idx, action) in self.actions.iter().enumerate() {
            content += &format!("{:23}", action);
            if (idx + 1) % 4 == 0 {
                content += "\n";
            }
        }
        content += "\n";
        print(&content);
        let buff_len = content.split('\n').collect::<Vec<_>>().len();
        if buff_len != self.len {
            self.len = buff_len;
            self.pos.1 = self.pos.0 + self.len;
        }
        Ok(())
    }

    fn set_start_pos(&mut self, start_pos: usize) {
        if start_pos != self.pos.0 {
            self.pos.0 = start_pos;
            self.pos.1 = self.pos.0 + self.len;
        }
    }

    fn start_pos(&self) -> usize {
        self.pos.0
    }

    fn end_pos(&self) -> usize {
        self.pos.1
    }

    fn len(&self) -> usize {
        self.len
    }
}
