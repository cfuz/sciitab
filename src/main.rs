#[macro_use]
extern crate lazy_static;
use console::Term;
use controller::{Controller, IController};
use model::tab::Tab;
use std::cell::RefCell;
use std::rc::Rc;

mod controller;
mod model;
mod view;

lazy_static! {
    static ref TERM: Term = Term::stdout();
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let tab = Rc::new(RefCell::new(Tab::new()));
    let mut ctrl: Controller = controller::initiator::Initiator::new(&tab).into();
    loop {
        let next = ctrl.fire()?;
        if next.is_some() {
            ctrl = next.unwrap();
        } else {
            break;
        }
    }
    Ok(())
}
