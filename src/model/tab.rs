#[derive(Debug)]
pub struct Tab {
    pub name: String,
    pub content: Vec<Vec<String>>,
    pub num_cols: usize,
    pub widths: Vec<usize>,
    pub header: bool,
    pub current: (usize, usize),
}

impl Tab {
    pub fn new() -> Self {
        Self {
            content: vec![],
            name: "?".to_string(),
            num_cols: 0,
            widths: vec![],
            current: (0, 0),
            header: false,
        }
    }

    pub fn toggle_header(&mut self) {
        if !self.header {
            if self.current.0 != 0 {
                self.content.swap(0, self.current.0);
                self.current.0 = 0;
            }
            self.header = true;
        } else {
            self.header = false;
            if self.current.0 != 0 {
                self.content.swap(0, self.current.0);
                self.current.0 = 0;
                self.header = true;
            }
        }
    }

    pub fn add_col_empty(&mut self) {
        self.num_cols += 1;
        self.widths.push(1);
        // Resizing content w/ newly inserted column
        for row in self.content.iter_mut() {
            row.push(Default::default());
        }
    }

    pub fn add_col(&mut self, mut col: Vec<String>) {
        self.widths.push(col.iter().fold(
            1,
            |acc, data| {
                if acc < data.len() {
                    data.len()
                } else {
                    acc
                }
            },
        ));
        self.num_cols += 1;
        if self.content.len() > col.len() {
            col.resize(self.content.len(), Default::default());
        } else if self.content.len() < col.len() {
            self.content
                .resize(col.len(), vec![Default::default(); self.num_cols - 1]);
        }
        for (idx, row) in self.content.iter_mut().enumerate() {
            row.push(col[idx].clone());
        }
    }

    pub fn del_col(&mut self) {
        self.num_cols -= 1;
        for row in self.content.iter_mut() {
            row.remove(self.current.1);
        }
        self.widths.remove(self.current.1);
        if self.current.1 == self.content[0].len() - 1 && self.current.1 != 0 {
            self.current.1 -= 1;
        }
    }

    pub fn add_row_empty(&mut self) {
        let mut row: Vec<String> = vec![];
        row.resize(self.num_cols, Default::default());
        self.content.push(row);
    }

    pub fn add_row(&mut self, mut row: Vec<String>) {
        if self.widths.is_empty() {
            self.num_cols = row.len();
            self.widths = vec![1; row.len()];
        }
        if self.num_cols < row.len() {
            self.num_cols = row.len();
            self.widths.resize_with(row.len(), || 1);
            self.content
                .resize(row.len(), vec![Default::default(); self.num_cols]);
        } else if self.num_cols > row.len() {
            row.resize(self.num_cols, Default::default());
        }
        for (idx, cell) in row.iter().enumerate() {
            if self.widths[idx] < cell.len() {
                self.widths[idx] = cell.len()
            }
        }
        self.content.push(row);
    }

    pub fn del_row(&mut self) {
        let num_lines = self.content.len();
        self.content.remove(self.current.0);
        self.resize();
        if self.current.0 == num_lines - 1 && self.current.0 != 0 {
            self.current.0 -= 1;
        }
    }

    pub fn resize(&mut self) {
        self.widths = vec![1; self.widths.len()];
        for row in self.content.iter() {
            for (idx, cell) in row.iter().enumerate() {
                if self.widths[idx] < cell.len() {
                    self.widths[idx] = cell.len();
                }
            }
        }
    }

    pub fn resize_col(&mut self, col_idx: usize) {
        self.widths[col_idx] = 1;
        for row in self.content.iter() {
            if self.widths[col_idx] < row[col_idx].len() {
                self.widths[col_idx] = row[col_idx].len();
            }
        }
    }

    pub fn clear_current(&mut self) {
        self.content[self.current.0][self.current.1].clear();
        self.resize_col(self.current.1);
    }

    pub fn move_current_up(&mut self) {
        if self.content.len() != 0 {
            if self.current.0 == 0 {
                self.current.0 = self.content.len() - 1;
            } else {
                self.current.0 -= 1;
            }
        }
    }

    pub fn move_current_down(&mut self) {
        if self.content.len() != 0 {
            if self.current.0 == self.content.len() - 1 {
                self.current.0 = 0;
            } else {
                self.current.0 += 1;
            }
        }
    }

    pub fn move_current_left(&mut self) {
        if self.content.len() != 0 && self.content[0].len() != 0 {
            if self.current.1 == 0 {
                self.current.1 = self.content[0].len() - 1;
            } else {
                self.current.1 -= 1;
            }
        }
    }

    pub fn move_current_right(&mut self) {
        if self.content.len() != 0 && self.content[0].len() != 0 {
            if self.current.1 == self.content[0].len() - 1 {
                self.current.1 = 0;
            } else {
                self.current.1 += 1;
            }
        }
    }

    pub fn update_current(&mut self, data: &str) {
        self.content[self.current.0][self.current.1] = data.to_string();
        if self.widths[self.current.1] < data.len() {
            self.widths[self.current.1] = data.len();
        }
    }

    pub fn current_field(&self) -> Option<&String> {
        if self.header {
            Some(&self.content[0][self.current.1])
        } else {
            None
        }
    }

    pub fn current_row(&self) -> &Vec<String> {
        &self.content[self.current.0]
    }

    pub fn current(&self) -> &String {
        &self.content[self.current.0][self.current.1]
    }

    pub fn is_header_activ(&self) -> bool {
        self.current.0 == 0
    }

    pub fn is_empty(&self) -> bool {
        self.content.is_empty()
    }
}
