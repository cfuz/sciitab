use super::{initiator::Initiator, Controller, IController};
use crate::model::tab::Tab;
use crate::view::{actions::Actions, ascii_tab::AsciiTab, title::Title, IView, View};
use crate::TERM;
use dialoguer::{theme::ColorfulTheme, Input};
use std::cell::RefCell;
use std::rc::Rc;

const ACTIONS: [&str; 11] = [
    "[ 1 ] Push row (empty)",
    "[ ! ] Push row (init.)",
    "[ 2 ] Del row",
    "[ 3 ] Push col (empty)",
    "[ # ] Push col (init.)",
    "[ 4 ] Del col",
    "[ 5 ] Toggle header",
    "[Ret] Clear",
    "[Ent] Update",
    "[ S ] Save",
    "[ Q ] Quit",
];

pub struct Menu {
    comps: [View; 3],
    model: Rc<RefCell<Tab>>,
}

impl Menu {
    pub fn new(model: &Rc<RefCell<Tab>>) -> Self {
        Self {
            comps: [
                Title::new().into(),
                AsciiTab::new(&model).into(),
                Actions::new(&ACTIONS.iter().map(|x| x.to_string()).collect()).into(),
            ],
            model: model.clone(),
        }
    }
}

impl IController for Menu {
    fn comps_mut(&mut self) -> Vec<&mut View> {
        self.comps.iter_mut().collect::<Vec<_>>()
    }

    fn fire(&mut self) -> Result<Option<Controller>, Box<dyn std::error::Error>> {
        self.init_comps()?;
        let mut quit = false;
        let next = loop {
            if self.model.borrow().is_empty() {
                break Ok(Some(Initiator::new(&self.model).into()));
            }
            if quit {
                break Ok(None);
            }
            TERM.move_cursor_to(0, self.comps[0].len())?;
            TERM.hide_cursor()?;
            TERM.clear_to_end_of_screen()?;
            for comp in self.comps[1..].iter_mut() {
                comp.show()?;
            }
            self.comps[2].set_start_pos(self.comps[2].end_pos());
            let input = TERM.read_key()?;
            // TERM.write_line(&format!("{:?}", &input))?;
            // std::thread::sleep(std::time::Duration::from_millis(1000));
            match input {
                console::Key::ArrowUp => self.model.borrow_mut().move_current_up(),
                console::Key::ArrowDown => self.model.borrow_mut().move_current_down(),
                console::Key::ArrowLeft => self.model.borrow_mut().move_current_left(),
                console::Key::ArrowRight => self.model.borrow_mut().move_current_right(),
                console::Key::Enter => {
                    TERM.clear_last_lines(self.comps[2].len())?;
                    let val = Input::<String>::with_theme(&ColorfulTheme {
                        prompt_prefix: console::style(
                            if let Some(field) = self.model.borrow().current_field() {
                                format!(
                                    "{0}{2}{1} ?",
                                    "[ ",
                                    " ]",
                                    format!(
                                        "field: {}",
                                        if field.is_empty() {
                                            "-".to_string()
                                        } else {
                                            field.clone()
                                        }
                                    )
                                )
                            } else {
                                "?".to_string()
                            },
                        )
                        .yellow(),
                        ..Default::default()
                    })
                    .with_prompt(if self.model.borrow().is_header_activ() {
                        "Field"
                    } else {
                        "Value"
                    })
                    .with_initial_text(&self.model.borrow().current().clone())
                    .interact()?;
                    self.model.borrow_mut().update_current(&val);
                }
                console::Key::Backspace => {
                    self.model.borrow_mut().clear_current();
                }
                console::Key::Char(c) => match c.to_ascii_lowercase() {
                    '1' => {
                        self.model.borrow_mut().add_row_empty();
                    }
                    '!' => {
                        TERM.clear_last_lines(self.comps[2].len())?;
                        let data = Input::<String>::with_theme(&ColorfulTheme::default())
                            .with_prompt("Values [sep.: \\t]")
                            .interact()?
                            .split('\t')
                            .map(|x| x.to_string())
                            .collect::<Vec<String>>();
                        self.model.borrow_mut().add_row(data);
                    }
                    '2' => {
                        self.model.borrow_mut().del_row();
                    }
                    '3' => {
                        self.model.borrow_mut().add_col_empty();
                    }
                    '#' => {
                        TERM.clear_last_lines(self.comps[2].len())?;
                        let data = Input::<String>::with_theme(&ColorfulTheme::default())
                            .with_prompt("Values [sep.: \\t]")
                            .interact()?
                            .split('\t')
                            .map(|x| x.to_string())
                            .collect::<Vec<String>>();
                        self.model.borrow_mut().add_col(data);
                    }
                    '4' => {
                        self.model.borrow_mut().del_col();
                    }
                    '5' => {
                        self.model.borrow_mut().toggle_header();
                    }
                    's' => {
                        TERM.clear_last_lines(self.comps[2].len())?;
                        TERM.write_line("Work in progress!")?;
                        std::thread::sleep(std::time::Duration::from_millis(1000));
                    }
                    'q' => {
                        TERM.clear_last_lines(self.comps[2].len())?;
                        if dialoguer::Confirm::with_theme(&ColorfulTheme::default())
                            .with_prompt("Exit the application ?")
                            .default(true)
                            .interact()? 
                        {
                            quit = true;
                        }
                    }
                    _ => (),
                },
                _ => (),
            }
        };
        TERM.move_cursor_to(0, 0)?;
        TERM.clear_to_end_of_screen()?;
        next
    }
}
