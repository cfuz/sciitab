use super::{menu::Menu, Controller, IController};
use crate::model::tab::Tab;
use crate::view::{ascii_tab::AsciiTab, title::Title, IView, View};
use crate::TERM;
use dialoguer::{theme::ColorfulTheme, Confirm, Input};
use std::cell::RefCell;
use std::rc::Rc;

const MULTI_SELECTION: [&str; 2] = ["Header", "Empty"];

pub struct Initiator {
    comps: [View; 2],
    model: Rc<RefCell<Tab>>,
}

impl Initiator {
    pub fn new(model: &Rc<RefCell<Tab>>) -> Self {
        Self {
            comps: [Title::new().into(), AsciiTab::new(&model).into()],
            model: model.clone(),
        }
    }
}

impl IController for Initiator {
    fn comps_mut(&mut self) -> Vec<&mut View> {
        self.comps.iter_mut().collect::<Vec<_>>()
    }

    fn fire(&mut self) -> Result<Option<Controller>, Box<dyn std::error::Error>> {
        TERM.clear_screen()?;
        self.init_comps()?;
        let mut num_questions: usize = 0;
        let next = {
            if !Confirm::with_theme(&ColorfulTheme::default())
                .with_prompt("Init. with empty row ?")
                .default(false)
                .interact()
                .unwrap()
            {
                let data = Input::<String>::with_theme(&ColorfulTheme::default())
                    .with_prompt("Values [sep.: \\t]")
                    .interact()?
                    .split('\t')
                    .map(|x| x.to_string())
                    .collect::<Vec<String>>();
                let mut tab = self.model.borrow_mut();
                tab.add_row(data);
            } else {
                let num_cols = Input::<String>::with_theme(&ColorfulTheme::default())
                    .with_prompt("Num. cols")
                    .validate_with(|input: &str| -> Result<(), &str> {
                        if input.parse::<usize>().is_ok() {
                            if input.parse::<usize>().unwrap() > 0 {
                                Ok(())
                            } else {
                                Err("Expected a number greater than 0!")
                            }
                        } else {
                            Err("NaN!")
                        }
                    })
                    .interact()?
                    .parse::<usize>()?;
                self.model
                    .borrow_mut()
                    .add_row((0..num_cols).fold(vec![], |mut acc, _idx| {
                        acc.push("".to_string());
                        acc
                    }));
            }
            num_questions += 2;
            if Confirm::with_theme(&ColorfulTheme::default())
                .with_prompt("Define as header ?")
                .default(true)
                .interact()
                .unwrap()
            {
                self.model.borrow_mut().toggle_header();
            }
            num_questions += 1;
            self.model.borrow_mut().name = Input::<String>::with_theme(&ColorfulTheme::default())
                .with_prompt("Tab. name")
                .interact()?;
            num_questions += 1;
            Ok(Some(Menu::new(&self.model).into()))
        };
        TERM.move_cursor_to(0, self.comps.last().unwrap().end_pos() + num_questions)?;
        TERM.clear_last_lines(
            self.comps.iter().fold(0, |acc, view| acc + view.len()) + num_questions,
        )?;
        next
    }
}
