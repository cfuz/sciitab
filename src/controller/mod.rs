use crate::view::{IView, View};
use crate::TERM;
use enum_dispatch::enum_dispatch;
use initiator::Initiator;
use menu::Menu;

pub(crate) mod initiator;
pub(crate) mod menu;

#[enum_dispatch]
pub enum Controller {
    Initiator,
    Menu,
}

#[enum_dispatch(Controller)]
pub trait IController {
    fn fire(&mut self) -> Result<Option<Controller>, Box<dyn std::error::Error>>;

    fn comps_mut(&mut self) -> Vec<&mut View>;

    fn init_comps(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        TERM.move_cursor_to(0, 0)?;
        // Spawning view components to initiate their len and starting pos.
        let mut end_pos: usize = 0;
        for comp in self.comps_mut().iter_mut() {
            comp.set_start_pos(end_pos);
            comp.show()?;
            end_pos = comp.end_pos();
        }
        Ok(())
    }

    fn update_comps(&mut self, from: usize) -> Result<(), Box<dyn std::error::Error>> {
        {
            // Spawning view components to initiate their len and starting pos.
            let mut end_pos: usize = 0;
            for comp in self.comps_mut()[from..].iter_mut() {
                comp.set_start_pos(end_pos);
                comp.show()?;
                end_pos = comp.end_pos();
            }
        };
        Ok(())
    }
}
